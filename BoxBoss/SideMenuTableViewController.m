//
//  SideMenuTableViewController.m
//  BoxBoss
//
//  Created by ark on 25/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SideMenuTableViewController.h"
#import "SideMenuTableViewCell.h"
#import "FeedbackViewController.h"
#import "BrowseLocationViewController.h"
#import "SWRevealViewController.h"
#import "UIColor+expanded.h"

@interface SideMenuTableViewController ()
{
    BOOL isSignedIn;
}
@end

@implementation SideMenuTableViewController
{
    NSArray *menuItems;
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [self checkSignedInUser];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    menuItems = @[@"Loyalty Points",@"Catalogue",@"Feedback",@"Notification",@"Merchant",@"About Us",@"Sign In / Sign Out"];
    self.tableView.bounces = NO;
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"232323"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView reloadData];
}

- (void) checkSignedInUser{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"authToken"] == nil) {
        NSLog(@"THERE IS NO AUTH TOKEN");
        isSignedIn = NO;
    }
    else{
        isSignedIn = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 43;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [UIView alloc];
    if (section==0){
        headerView = [headerView initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
        headerView.backgroundColor = [UIColor colorWithHexString:@"2E2E2E"];
        UILabel * lblRes = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 30)];
        lblRes.text = @"Box Boss";
        lblRes.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
        lblRes.textColor = [UIColor whiteColor];
        [headerView addSubview:lblRes];
    }
    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SideMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[SideMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        [cell setupView];
    }
    cell.backgroundColor = [UIColor colorWithHexString:@"232323"];
    [cell loadViewWithItemName:[menuItems objectAtIndex:indexPath.row] iconIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SWRevealViewController *revealController = self.revealViewController;

    if (indexPath.row == 0){
        if (isSignedIn){
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loyalty"];
        [revealController pushFrontViewController:navController animated:YES];
        }
        else{
            UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Not signed in" message:@"Loyalty points is only available for signed in users" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alrt show];
        }
    }
    else if (indexPath.row == 1){
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"cate"];
        [revealController pushFrontViewController:navController animated:YES];
    }
    else if (indexPath.row ==2){
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"feedback"];
        [revealController pushFrontViewController:navController animated:YES];
    }
    else if (indexPath.row ==3){
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"notification"];
        [revealController pushFrontViewController:navController animated:YES];
    }
    else if (indexPath.row ==4){
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"merchant"];
        [revealController pushFrontViewController:navController animated:YES];
    }
    else if (indexPath.row ==5){
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"about"];
        [revealController pushFrontViewController:navController animated:YES];
    }
    else if (indexPath.row ==6){
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"sign"];
        [revealController pushFrontViewController:navController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        SWRevealViewController *revealController = self.revealViewController;
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"sign"];
        [revealController pushFrontViewController:navController animated:YES];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
