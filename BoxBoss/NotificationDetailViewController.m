//
//  NotificationDetailViewController.m
//  Box Boss
//
//  Created by ark on 6/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NotificationDetailViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface NotificationDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;

@end

@implementation NotificationDetailViewController
@synthesize notif;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}

- (void)viewWillAppear:(BOOL)animated{
    [self populateView];
}

- (void) setupView{
    self.imgView.layer.cornerRadius = 5;
    self.imgView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void) populateView{
    self.lblTitle.text = notif.name;
    self.lblDate.text = @"";
    //self.lblDate.text = [NSString stringWithFormat:@"Date Posted: %@",notif.date];
    self.txtDescription.text = notif.descNotif;
    [self.imgView setImageWithURL:[NSURL URLWithString:notif.image]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
