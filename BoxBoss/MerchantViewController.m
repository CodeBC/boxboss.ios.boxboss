//
//  MerchantViewController.m
//  Box Boss
//
//  Created by Arkar Aung on 8/20/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MerchantViewController.h"
#import "SWRevealViewController.h"
#import "APIClient.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SVProgressHUD.h"

@interface MerchantViewController ()
{
    NSString * aboutImage;
    NSString * aboutText;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnMenu;
@property (weak, nonatomic) IBOutlet UIWebView *webViewText;

@end

@implementation MerchantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadData];
    self.navigationItem.title = @"Merchant";
    
    _btnMenu.target = self.revealViewController;
    _btnMenu.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void) loadData{
    NSString *fullURL = @"http://boxboss.ddns.me/web/default.aspx";
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webViewText loadRequest:requestObj];
}

- (void) populateView{
    [self populateWebView];
}

- (void) populateWebView{
    NSMutableString *html = [NSMutableString stringWithString: aboutText];
    [self.webViewText setBackgroundColor:[UIColor clearColor]];
    [self.webViewText loadHTMLString:[html description] baseURL:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
