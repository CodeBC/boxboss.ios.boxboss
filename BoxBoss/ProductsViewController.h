//
//  ProductsViewController.h
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductsViewController : UIViewController  <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong,nonatomic) NSNumber *categoryID;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;
@property (strong, nonatomic) NSString *catLblName;
@end
