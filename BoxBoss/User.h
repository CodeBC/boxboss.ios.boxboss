//
//  User.h
//  Box Boss
//
//  Created by ark on 30/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic,strong) NSString * userID;

@end
