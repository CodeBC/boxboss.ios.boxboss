//
//  ProductCell.h
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"

@interface ProductCell : UICollectionViewCell

- (void) setupView;
- (void) loadViewWithProductObject:(Products *)objProd;

@end
