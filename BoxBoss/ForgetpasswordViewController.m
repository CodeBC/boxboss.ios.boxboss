//
//  ForgetpasswordViewController.m
//  Box Boss
//
//  Created by ark on 30/5/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ForgetpasswordViewController.h"
#import "APIClient.h"
#import "SVProgressHUD.h"
#import "UIColor+expanded.h"

@interface ForgetpasswordViewController (){
    NSString * strEmail;
    BOOL goodToGo;
}
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UILabel *txtNotifMessage;

@end

@implementation ForgetpasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.txtEmail.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self setupView];
    goodToGo = NO;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtEmail) {
        [self.txtEmail resignFirstResponder];
    }
    return YES;
}

- (void) setupView{
    self.txtEmail.layer.borderWidth = 1;

    self.txtEmail.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtEmail.layer.cornerRadius = 5;
    self.txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    self.btnSubmit.layer.cornerRadius = 5;
    
    self.txtNotifMessage.layer.cornerRadius = 5;
}

- (IBAction)tappedSubmit:(id)sender {
    [self validateFields];
    if (goodToGo) [self sendData];
}

- (void) validateFields{
    strEmail = self.txtEmail.text;
    if ([strEmail isEqualToString:@""]){
        goodToGo = NO;
        UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Email Required" message:@"Please enter email that you have registered." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alrt show];
    }
    else{
        goodToGo = YES;
    }
}

- (void)sendData{
    [SVProgressHUD showWithStatus:@"Submitting"];
    NSDictionary * param = @{@"email": strEmail};
    [[APIClient sharedClient] POST:@"users/forget_password" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSInteger statusCode = operation.response.statusCode;

        [SVProgressHUD dismiss];
       
        if(statusCode == 200){
            UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Email Sent" message:@"Please check your email. Change password from the link in the email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alrtSuccess show];
        }
        else if(statusCode == 400) {
            UIAlertView *alrtWrong = [[UIAlertView alloc]initWithTitle:@"Invalid" message:@"Invalid email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alrtWrong show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
        UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:@"Failed" message:@"Fail to send email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alrt show];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
