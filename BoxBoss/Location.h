//
//  Location.h
//  BoxBoss
//
//  Created by ark on 26/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject


@property (nonatomic,strong) NSNumber * locID;
@property (nonatomic,strong) NSString * locName;
@property (nonatomic,strong) NSString * locImgURL;

@end
