//
//  NotificationDetailViewController.h
//  Box Boss
//
//  Created by ark on 6/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notification.h"

@interface NotificationDetailViewController : UIViewController

@property (strong,nonatomic) Notification *notif;

@end
