//
//  Products.h
//  BoxBoss
//
//  Created by ark on 26/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Products : NSObject

@property (nonatomic,strong) NSNumber * prodID;
@property (nonatomic,strong) NSString * prodName;
@property (nonatomic,strong) NSString * prodPrice;
@property (nonatomic,strong) NSString * prodDescription;
@property (nonatomic,strong) NSString * prodImage;
@property (nonatomic,strong) NSString * prodImgURL;
@end
