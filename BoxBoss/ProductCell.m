//
//  ProductCell.m
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProductCell.h"
#import "Products.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation ProductCell
{
    UILabel *lblProductName;
    UIImageView *imgViewProduct;
    UIView *overlayView;
    UIImageView *imgViewArrow;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setupView{
    if (!imgViewProduct){
        self.backgroundColor = [UIColor whiteColor];
        imgViewProduct = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 140, 100)];
        imgViewProduct.contentMode = UIViewContentModeRedraw;
        [self addSubview:imgViewProduct];
    }
    
    if (!overlayView){
        overlayView = [[UIView alloc]initWithFrame:CGRectMake(0, 76, 145, 30)];
        overlayView.backgroundColor = [UIColor blackColor];
        overlayView.alpha = 0.5;
        [self addSubview:overlayView];
    }
    
    if (!lblProductName){
        lblProductName = [[UILabel alloc] initWithFrame:CGRectMake(10, 77, 110, 20)];
        lblProductName.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
        lblProductName.textColor = [UIColor whiteColor];
        [self addSubview:lblProductName];
    }
    
    if (!imgViewArrow){
        imgViewArrow = [[UIImageView alloc]initWithFrame:CGRectMake(120, 80, 15, 15)];
        imgViewArrow.contentMode = UIViewContentModeScaleAspectFit;
        imgViewArrow.image = [UIImage imageNamed:@"arrow"];
        [self addSubview:imgViewArrow];
    }
}

- (void) loadViewWithProductObject:(Products *)objProd {
    [imgViewProduct setImageWithURL:[NSURL URLWithString:objProd.prodImgURL]];
    lblProductName.text = objProd.prodName;
}

@end
