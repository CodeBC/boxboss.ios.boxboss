//
//  BrowseCategoryViewController.h
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowseCategoryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong,nonatomic) NSNumber *locationID;

@end
