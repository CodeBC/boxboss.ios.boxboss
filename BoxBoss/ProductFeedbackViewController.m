//
//  ProductFeedbackViewController.m
//  Box Boss
//
//  Created by ark on 6/5/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProductFeedbackViewController.h"
#import "UIColor+expanded.h"
#import "APIClient.h"
#import "SVProgressHUD.h"
#import "ProductDetailViewController.h"

@interface ProductFeedbackViewController ()
{
    NSString * strFullName;
    NSString * strContactNo;
    NSString * strEmail;
    NSString * strMessage;
    NSString * strEnquiry;
    BOOL isSignedIn;
    UITextField *activeField;
    UITextView *activeTextView;
    BOOL goodToGo;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end


@implementation ProductFeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self setupView];
    self.navigationItem.title = @"Feedback";
    
}

- (void) viewWillAppear:(BOOL)animated{
    NSLog(@"GOT TO PROD with product ID %@",self.objProd.prodID);
    [self checkSignedInUser];
    if (isSignedIn) [self fillForm];
    [self registerForKeyboardNotifications];
}

- (void) viewWillDisappear:(BOOL)animated{
    [self deregisterFromKeyboardNotifications];
    [self dismissKeyboard];
}

- (void) fillForm{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.txtFullName.text = [defaults objectForKey:@"fullName"];
    self.txtContactNo.text = [defaults objectForKey:@"contactNo"];
    self.txtEmail.text = [defaults objectForKey:@"email"];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"Keyboard was show");
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if (activeField){
         NSLog(@"ACTIVE FIELD IS %@",activeField);
        CGRect bkgndRect = activeField.superview.frame;
        bkgndRect.size.height += kbSize.height;
        [activeField.superview setFrame:bkgndRect];
        
        if ((self.view.frame.size.height-(activeField.frame.origin.y+activeField.frame.size.height)) < kbSize.height){
            [self.scrollView setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
        }
    }
    if (activeTextView){
         NSLog(@"ACTIVE VIEW IS %@",activeTextView);
        CGRect bkgndRect = activeTextView.superview.frame;
        bkgndRect.size.height += kbSize.height;
        [activeTextView.superview setFrame:bkgndRect];
        
        if ((self.view.frame.size.height-(activeTextView.frame.origin.y+activeTextView.frame.size.height)) < kbSize.height){
            [self.scrollView setContentOffset:CGPointMake(0.0, activeTextView.frame.origin.y-kbSize.height) animated:YES];
        }
    }

    
}


- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void) checkSignedInUser{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"authToken"] == nil) {
        //NSLog(@"THERE IS NO AUTH TOKEN");
        isSignedIn = NO;
    }
    else {
        //NSLog(@"AUTH TOKEN");
        isSignedIn = YES;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
        if (activeField)
        {
            [activeField resignFirstResponder];
        }
        if (activeTextView){
            [activeTextView resignFirstResponder];
        }
    activeTextView = nil;
    activeField = textField;
   
    [activeField becomeFirstResponder];
    //NSLog(@"Active Field is %@",activeField);
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //NSLog(@"Active Field is %@",activeField);
    [activeField resignFirstResponder];
     activeField = nil;
    
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
        if (activeTextView){
            [activeTextView resignFirstResponder];
        }
        if (activeField)
        {
            [activeField resignFirstResponder];
        }
    
        activeField = nil;
        activeTextView = textView;
    
    if ([textView.text isEqualToString:@"Message"]) {
        textView.text = @"";
        textView.textColor = [UIColor whiteColor];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Message";
        textView.textColor = [UIColor whiteColor]; //optional
    }
      [activeTextView resignFirstResponder];
      activeTextView = nil;
}

- (void) setupView{
    self.txtFullName.delegate = self;
    self.txtContactNo.delegate = self;
    self.txtEmail.delegate = self;
    self.txtMessage.delegate = self;
    
    self.txtHeaderMessage.layer.cornerRadius = 5;
    self.txtMessage.layer.cornerRadius = 5;
    self.txtFullName.layer.cornerRadius = 5;
    self.txtContactNo.layer.cornerRadius = 5;
    self.txtEmail.layer.cornerRadius = 5;
    self.btnSubmit.layer.cornerRadius = 5;
    self.btnPickOne.layer.cornerRadius = 5;
    
    self.txtHeaderMessage.layer.borderWidth = 1;
    self.txtMessage.layer.borderWidth = 1;
    self.txtFullName.layer.borderWidth = 1;
    self.txtContactNo.layer.borderWidth = 1;
    self.txtEmail.layer.borderWidth = 1;
    self.btnPickOne.layer.borderWidth = 1;
    
    self.txtMessage.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtFullName.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtContactNo.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtEmail.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.btnPickOne.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    
    self.txtFullName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Fullname" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtContactNo.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contact No" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFullName) {
        [self.txtFullName resignFirstResponder];
        [self.txtContactNo becomeFirstResponder];
    } else if (textField == self.txtContactNo) {
        [self.txtContactNo resignFirstResponder];
        [self.txtEmail becomeFirstResponder];
    } else if (textField == self.txtEmail){
        [self.txtEmail resignFirstResponder];
        [self.txtMessage becomeFirstResponder];
    }
    return YES;
}

- (IBAction)tappedSubmit:(id)sender {
    [self validateFields];
    if (goodToGo) [self sendFeedback];
}

- (void) validateFields{
    NSLog(@"GOT TO PROD with product ID %@",self.objProd.prodID);
    strFullName = self.txtFullName.text;
    strContactNo = self.txtContactNo.text;
    strEmail = self.txtEmail.text;
    strMessage = self.txtMessage.text;
   
    if ([strFullName isEqualToString:@""] || [strContactNo isEqualToString:@""] || [strEmail isEqualToString:@""] || [strMessage isEqualToString:@""] || !strEnquiry)
    {
        UIAlertView *fieldAlrt = [[UIAlertView alloc]initWithTitle:@"Fields required" message:@"Please fill in all the fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [fieldAlrt show];
        goodToGo = NO;
    }
    else{
        goodToGo = YES;
    }
}

- (IBAction)tappedPickOne:(id)sender {
    [self.menu showInView:self.view];

}

- (UIActionSheet *)menu{
    if (!_menu) {
        _menu = [[UIActionSheet alloc] initWithTitle:@"Enquiry About"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"General Enquiry",@"Product Enquiry",nil];
    }
    return _menu;
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self.btnPickOne setTitle: @"General Enquiry" forState: UIControlStateNormal];
            strEnquiry = @"General Enquiry";
            break;
        case 1:
            [self.btnPickOne setTitle: @"Product Enquiry" forState: UIControlStateNormal];
            strEnquiry = @"Product Enquiry";
            break;
        default:
            break;
    }
}

-(void) sendFeedback{
    NSLog(@"HERE");
    
    [SVProgressHUD showWithStatus:@"Loading"];
    NSDictionary *params = @{@"full_name": strFullName,
                             @"contact_no": strContactNo,
                             @"email":strEmail,
                             @"message":strMessage,
                             @"product_id" : self.objProd.prodID,
                             @"enquiry_about" : strEnquiry};
    
    NSString * feedbackLink;
    if (isSignedIn){
        NSString * authToken = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"authToken"];
        
        feedbackLink = [NSString stringWithFormat:@"feedbacks?auth_token=%@",authToken];
    }
    else{
        feedbackLink = @"feedbacks";
    }
    
    [self dismissKeyboard];
    
    [[APIClient sharedClient] POST:feedbackLink parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [SVProgressHUD dismiss];
        UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Successfully submitted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alrtSuccess show];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSInteger status = operation.response.statusCode;
        if (status == 201) {
            [SVProgressHUD dismiss];
            UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Successfully submitted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alrtSuccess show];
        }
        else{
            [SVProgressHUD dismiss];
            UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Fail" message:@"Fail to submit feedback" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alrtSuccess show];
        }
    }];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
