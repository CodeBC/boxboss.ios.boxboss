//
//  AboutViewController.m
//  Box Boss
//
//  Created by ark on 13/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "AboutViewController.h"
#import "SWRevealViewController.h"
#import "APIClient.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SVProgressHUD.h"

@interface AboutViewController ()
{
    NSString * aboutImage;
    NSString * aboutText;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIWebView *webViewText;


@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadData];
    self.navigationItem.title = @"About Us";
    
    self.imgView.layer.cornerRadius = 5;
    self.imgView.contentMode = UIViewContentModeScaleAspectFit;
    
    _btnMenu.target = self.revealViewController;
    _btnMenu.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void) loadData{
    [SVProgressHUD showWithStatus:@"Loading"];
    [[APIClient sharedClient]GET:@"about" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        aboutImage = [responseObject objectForKey:@"image"];
        aboutText = [responseObject objectForKey:@"content"];
        [SVProgressHUD dismiss];
        [self populateView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

- (void) populateView{
    [self.imgView setImageWithURL:[NSURL URLWithString:aboutImage]];
    [self populateWebView];
}

- (void) populateWebView{
    NSMutableString *html = [NSMutableString stringWithString: aboutText];
    [self.webViewText setBackgroundColor:[UIColor clearColor]];
    [self.webViewText loadHTMLString:[html description] baseURL:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
