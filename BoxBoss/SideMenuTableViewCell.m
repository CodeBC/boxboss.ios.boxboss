//
//  SideMenuTableViewCell.m
//  BoxBoss
//
//  Created by ark on 25/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SideMenuTableViewCell.h"
#import "UIColor+expanded.h"

@implementation SideMenuTableViewCell
{
    UIImageView * sidebarIcons;
    UILabel * lblItemName;
    UIView * viewSeparator;
    NSArray * icons;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    icons = @[@"loyaltypoints",@"catalogue",@"feedback",@"notification", @"merchant", @"info",@"singup"];
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}


- (void) setupView{

    sidebarIcons = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 15, 15)];
    sidebarIcons.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:sidebarIcons];
    
    lblItemName = [[UILabel alloc] initWithFrame:CGRectMake(45, 0, self.frame.size.width-50, self.frame.size.height)];
    lblItemName.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
    lblItemName.textColor = [UIColor whiteColor];
    [self addSubview:lblItemName];
    
    viewSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, 43, self.frame.size.width, 1)];
    viewSeparator.backgroundColor =[UIColor colorWithHexString:@"303030"];
    [self addSubview:viewSeparator];
}

- (void) loadViewWithItemName:(NSString *)name iconIndex:(NSUInteger)index{
    lblItemName.text = name;
    sidebarIcons.image = [UIImage imageNamed:icons[index]];
}


- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:YES];

    // Configure the view for the selected state
}

@end
