//
//  Products.m
//  BoxBoss
//
//  Created by ark on 26/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Products.h"

@implementation Products
@synthesize prodID,
            prodName,
            prodPrice,
            prodDescription,
            prodImage,
            prodImgURL;
@end
