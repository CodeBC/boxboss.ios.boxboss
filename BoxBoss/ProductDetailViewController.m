//
//  ProductDetailViewController.m
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "Products.h"
#import "ProductFeedbackViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface ProductDetailViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnQuery;

@end

@implementation ProductDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self populateView];
}

- (void) setupView{
    self.imgViewProduct.layer.cornerRadius = 5;
    self.btnQuery.layer.cornerRadius = 5;
    self.lblProductName.text = self.productLblName;
 //   self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]style:UIBarButtonItemStylePlain target:nil action:nil];
}

-(void) populateView{
    self.lblPrice.text = [NSString stringWithFormat:@"Price: %@ USD",self.objProdDetail.prodPrice];
    self.txtDescription.text = self.objProdDetail.prodDescription;
    self.txtDescription.textColor = [UIColor whiteColor];
    self.imgViewProduct.contentMode = UIViewContentModeScaleAspectFit;
    [self.imgViewProduct setImageWithURL:[NSURL URLWithString:self.objProdDetail.prodImgURL]];
}

- (IBAction)tappedQuery:(id)sender {

    ProductFeedbackViewController * prodFeedbackVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductFeedbackViewController"];
    prodFeedbackVC.objProd = self.objProdDetail;
    [self.navigationController pushViewController:prodFeedbackVC animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
