//
//  FeedbackViewController.h
//  BoxBoss
//
//  Created by ark on 24/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"

@interface FeedbackViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnMenu;
@property (strong, nonatomic) Products * objProd;
@property (weak, nonatomic) IBOutlet UITextField *txtFullName;
@property (weak, nonatomic) IBOutlet UITextField *txtContactNo;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextView *txtMessage;
@property (weak, nonatomic) IBOutlet UILabel *txtHeaderMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@end
