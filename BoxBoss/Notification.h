//
//  Notification.h
//  Box Boss
//
//  Created by ark on 30/5/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject

@property (nonatomic,strong) NSNumber * notifID;
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * descNotif;
@property (nonatomic,strong) NSString * date;
@property (nonatomic,strong) NSString * image;

@end
