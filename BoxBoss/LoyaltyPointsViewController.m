//
//  LoyaltyPointsViewController.m
//  Box Boss
//
//  Created by ark on 29/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//


#import "LoyaltyPointsViewController.h"
#import "SWRevealViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SVProgressHUD.h"
#import "APIClient.h"

@interface LoyaltyPointsViewController ()
{
    BOOL isSignedIn;
}
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberID;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberName;
@property (weak, nonatomic) IBOutlet UILabel *lblBalancePoints;
@property (weak, nonatomic) IBOutlet FBProfilePictureView *profilePicView;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnMenu;

@end

@implementation LoyaltyPointsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];

    _btnMenu.target = self.revealViewController;
    _btnMenu.action = @selector(revealToggle:);
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.navigationItem.title = @"Loyalty Points";

}

- (void) viewWillAppear:(BOOL)animated{
    [self checkSignedInUser];
    if (isSignedIn){
        [self loadData];
    }
    else{
        NSLog(@"NOT A SIGNED IN USER");
        SWRevealViewController *revealController = self.revealViewController;
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"sign"];
        [revealController pushFrontViewController:navController animated:YES];
    }
}


- (void) checkSignedInUser{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"authToken"] == nil) {
        NSLog(@"THERE IS NO AUTH TOKEN");
        isSignedIn = NO;
    }
    else{
        isSignedIn = YES;
    }
}

- (void) loadData{
        NSString * loyaltyLink;
        if (isSignedIn){
            NSString * authToken = [[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"authToken"];
            NSLog(@"authToken is %@",authToken);
            loyaltyLink = [NSString stringWithFormat:@"loyalty_points?auth_token=%@",authToken];
        }
        else{
            loyaltyLink = @"loyalty_points";
        }
    NSLog(@"THE LINK IS %@",loyaltyLink);
        [SVProgressHUD showWithStatus:@"Loading"];

        [[APIClient sharedClient] GET:loyaltyLink parameters:nil success: ^(AFHTTPRequestOperation *operation, id json){
            
            self.profilePicView.profileID = [NSString stringWithFormat:@"%@",
                                                 [json objectForKey:@"fb_id"]];
            self.lblMemberName.text = [NSString stringWithFormat:@"%@",
                                                                   [json objectForKey:@"name"]];
            self.lblMemberID.text = [NSString stringWithFormat:@"%@",
                                                               [json objectForKey:@"id"]];
            self.lblBalancePoints.text = [NSString stringWithFormat:@"%@",                                                                               [json objectForKey:@"loyalty_points"]];
            
            NSLog(@"The response is %@",json);
            [SVProgressHUD dismiss];
        }failure:^(AFHTTPRequestOperation *operation, NSError *error){
            NSLog(@"THE ERROS is %@",error);
            NSInteger statusCode = operation.response.statusCode;
            if(statusCode == 401) {
                
                UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Guest User" message:@"Loyalty Points is not available for Guest User" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alrtSuccess show];
            }

            [SVProgressHUD dismiss];
        }];

}

- (void) setupView{

    self.viewBackground.layer.cornerRadius = 5;
    [self setRoundedView:self.profilePicView toDiameter:100.0];
    
}

-(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
