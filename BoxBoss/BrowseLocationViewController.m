//
//  BrowseLocationViewController.m
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BrowseLocationViewController.h"
#import "APIClient.h"
#import "BrowseCategoryViewController.h"
#import "LocationCell.h"
#import "SWRevealViewController.h"
#import "Location.h"
#import "SVProgressHUD.h"

@interface BrowseLocationViewController ()
{
    NSMutableArray * arrLocations;
    NSMutableArray * arrLocNames;
}
@property (weak, nonatomic) IBOutlet UICollectionView *locationCollectionView;

@end

@implementation BrowseLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{   arrLocations = [[NSMutableArray alloc]init];
    [super viewDidLoad];
    [self loadData];
    // Change button color
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _btnMenu.target = self.revealViewController;
    _btnMenu.action = @selector(revealToggle:);
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.navigationItem.title = @"Location";
    self.locationCollectionView.dataSource = self;
    self.locationCollectionView.delegate = self;
    
}

- (void) loadData{
    [SVProgressHUD showWithStatus:@"Loading"];
    [[APIClient sharedClient] GET:@"locations" parameters:nil success: ^(AFHTTPRequestOperation *operation, id json){
        for (NSDictionary *dictLoc in json){
            Location * objLoc = [[Location alloc]init];
            objLoc.locID = [dictLoc objectForKey:@"id"];
            objLoc.locName = [dictLoc objectForKey:@"name"];
            objLoc.locImgURL = [dictLoc objectForKey:@"image"];
            [arrLocations addObject:objLoc];
        }
        [self.locationCollectionView reloadData];
        [SVProgressHUD dismiss];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"THE ERROS is %@",error);
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [arrLocations count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    LocationCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"locationCell" forIndexPath:indexPath];
    LocationCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"locationCell" forIndexPath:indexPath];
    cell.layer.cornerRadius = 5;
    [cell setupView];
    [cell loadViewWithLocationObject:[arrLocations objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Location * objLoc;
    objLoc = [arrLocations objectAtIndex:indexPath.row];
    BrowseCategoryViewController * categoryVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"cateViewController"];
    categoryVC.locationID = objLoc.locID;
    [self.navigationController pushViewController:categoryVC animated:YES];

    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(250, 100);
    return retval;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 15, 15, 15);
}

@end
