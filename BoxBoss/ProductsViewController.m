//
//  ProductsViewController.m
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProductsViewController.h"
#import "ProductDetailViewController.h"
#import "ProductCell.h"
#import "Products.h"
#import "APIClient.h"
#import "SVProgressHUD.h"

@interface ProductsViewController ()
{
    NSMutableArray * arrProducts;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewProducts;
@end

@implementation ProductsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    arrProducts = [[NSMutableArray alloc]init];
    [super viewDidLoad];
    self.navigationItem.title = @"Products";
    [self loadData];
    self.lblCategoryName.text = self.catLblName;
    self.collectionViewProducts.dataSource = self;
    self.collectionViewProducts.delegate = self;

    self.automaticallyAdjustsScrollViewInsets = NO;
   // self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void) loadData{
    [SVProgressHUD showWithStatus:@"Loading"];
    NSDictionary *params = @{@"category_id": self.categoryID};
    
    [[APIClient sharedClient] POST:@"products" parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        for (NSDictionary *dictProd in json){
            Products * objProd = [[Products alloc]init];
            objProd.prodID = [dictProd objectForKey:@"id"];
            objProd.prodName = [dictProd objectForKey:@"name"];
            objProd.prodPrice = [dictProd objectForKey:@"price"];
            objProd.prodDescription = [dictProd objectForKey:@"description"];
            objProd.prodImgURL = [dictProd objectForKey:@"image"];
            [arrProducts addObject:objProd];
        }
        [self.collectionViewProducts reloadData];
        [SVProgressHUD dismiss];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"THE ERROS is %@",error);
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [arrProducts count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ProductCell" forIndexPath:indexPath];
    cell.layer.cornerRadius = 5;
    [cell setupView];
    [cell loadViewWithProductObject:[arrProducts objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Products * objProd;
    objProd = [arrProducts objectAtIndex:indexPath.row];
    ProductDetailViewController * productDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"prodctDetailViewController"];
    productDetailVC.productLblName = objProd.prodName;
    productDetailVC.objProdDetail = objProd;
    [self.navigationController pushViewController:productDetailVC animated:YES];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(140, 100);
    return retval;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 15, 15, 15);
}

@end
