//
//  ProductDetailViewController.h
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"

@interface ProductDetailViewController : UIViewController

@property (nonatomic,strong) Products * objProdDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (strong, nonatomic) NSString * productLblName;

@end
