//
//  Notification.m
//  Box Boss
//
//  Created by ark on 30/5/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Notification.h"

@implementation Notification
@synthesize notifID,
            name,
            descNotif,
            date,
            image;
@end
