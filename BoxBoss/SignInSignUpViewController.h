//
//  SignInSignUpViewController.h
//  BoxBoss
//
//  Created by ark on 26/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface SignInSignUpViewController : UIViewController <UITextFieldDelegate,FBLoginViewDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet FBLoginView *loginView;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@end
