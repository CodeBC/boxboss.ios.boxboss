//
//  CategoryCell.m
//  BoxBoss
//
//  Created by ark on 24/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "CategoryCell.h"
#import "CategoriesByLocation.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation CategoryCell
{
    UILabel *lblCategoryName;
    UIImageView *imgViewCategory;
    UIView *overlayView;
    UIImageView *imgViewArrow;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setupView{
    if (!imgViewCategory){
        self.backgroundColor = [UIColor whiteColor];
        imgViewCategory = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 140, 100)];
        imgViewCategory.contentMode = UIViewContentModeRedraw;
        [self addSubview:imgViewCategory];
    }
    
    if (!overlayView){
        overlayView = [[UIView alloc]initWithFrame:CGRectMake(0, 76, 145, 30)];
        overlayView.backgroundColor = [UIColor blackColor];
        overlayView.alpha = 0.5;
        [self addSubview:overlayView];
    }
    
    if (!imgViewArrow){
        imgViewArrow = [[UIImageView alloc]initWithFrame:CGRectMake(120, 80, 15, 15)];
        imgViewArrow.contentMode = UIViewContentModeScaleAspectFit;
        imgViewArrow.image = [UIImage imageNamed:@"arrow"];
        [self addSubview:imgViewArrow];
    }
    
    if (!lblCategoryName){
        lblCategoryName = [[UILabel alloc] initWithFrame:CGRectMake(10, 77, 110, 20)];
        lblCategoryName.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
        lblCategoryName.text = @"Supercars";
        lblCategoryName.textColor = [UIColor whiteColor];
        [self addSubview:lblCategoryName];
    }

    
}

- (void) loadViewWithCategoryObject:(CategoriesByLocation *)objCat{
    [imgViewCategory setImageWithURL:[NSURL URLWithString:objCat.catImgURL] placeholderImage:nil options:SDWebImageRefreshCached];
    lblCategoryName.text = objCat.catName;
}
@end
