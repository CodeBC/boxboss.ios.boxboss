//
//  LocationCell.h
//  BoxBoss
//
//  Created by ark on 24/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"

@interface LocationCell : UICollectionViewCell

- (void) setupView;
- (void) loadViewWithLocationObject:(Location *)objLoc;
@end
