//
//  NotificationViewController.h
//  BoxBoss
//
//  Created by ark on 24/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
