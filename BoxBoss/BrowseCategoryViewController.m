//
//  BrowseCategoryViewController.m
//  BoxBoss
//
//  Created by ark on 21/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BrowseCategoryViewController.h"
#import "ProductsViewController.h"
#import "CategoryCell.h"
#import "APIClient.h"
#import "CategoriesByLocation.h"
#import "SVProgressHUD.h"

@interface BrowseCategoryViewController ()
{
    NSMutableArray * arrCategory;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewCategory;

@end

@implementation BrowseCategoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    arrCategory = [[NSMutableArray alloc]init];
    [super viewDidLoad];
    self.navigationItem.title = @"Category";
    [self loadData];
    self.collectionViewCategory.dataSource = self;
    self.collectionViewCategory.delegate = self;
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"]style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void) loadData{
    [SVProgressHUD showWithStatus:@"Loading"];
    NSDictionary *params = @{@"location_id": self.locationID};

    [[APIClient sharedClient] POST:@"categories" parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        for (NSDictionary *dictCat in json){
            CategoriesByLocation * objCat = [[CategoriesByLocation alloc]init];
            objCat.catID = [dictCat objectForKey:@"id"];
            objCat.catName = [dictCat objectForKey:@"name"];
            objCat.catImgURL = [dictCat objectForKey:@"image"];
            [arrCategory addObject:objCat];
        }
        [self.collectionViewCategory reloadData];
        [SVProgressHUD dismiss];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"THE ERROS is %@",error);
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [arrCategory count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"CategoryCell" forIndexPath:indexPath];
    cell.layer.cornerRadius = 5;
    [cell setupView];
    [cell loadViewWithCategoryObject:[arrCategory objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoriesByLocation * objCat;
    objCat = [arrCategory objectAtIndex:indexPath.row];
    ProductsViewController * productVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"productViewController"];
    productVC.catLblName = objCat.catName;
    productVC.categoryID = objCat.catID;
    [self.navigationController pushViewController:productVC animated:YES];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(140, 100);
    return retval;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 15, 15, 15);
}


@end
