//
//  SingUpViewController.h
//  BoxBoss
//
//  Created by ark on 26/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingUpViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@end