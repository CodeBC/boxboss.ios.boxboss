//
//  SingUpViewController.m
//  BoxBoss
//
//  Created by ark on 26/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SingUpViewController.h"
#import "SVProgressHUD.h"
#import "APIClient.h"
#import "UIColor+expanded.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"

@interface SingUpViewController ()
{
    UITextField *activeField;
}
@property (weak, nonatomic) IBOutlet UITextField *txtFullName;
@property (weak, nonatomic) IBOutlet UITextField *txtContactNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPass;

@end

@implementation SingUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.txtFullName.delegate = self;
    self.txtContactNumber.delegate = self;
    self.txtEmail.delegate = self;
    self.txtPassword.delegate = self;
    self.txtConfirmPass.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self setupView];
}


- (void) viewWillAppear:(BOOL)animated{
    [self registerForKeyboardNotifications];
}

- (void) viewWillDisappear:(BOOL)animated{
    [self deregisterFromKeyboardNotifications];
    [self dismissKeyboard];
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"Keyboard shown");
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    [activeField.superview setFrame:bkgndRect];

    if ((self.view.frame.size.height-(activeField.frame.origin.y+activeField.frame.size.height) < kbSize.height)){
        [self.scrollView setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (activeField)
    {
        [activeField resignFirstResponder];
    }
    activeField = textField;
    [activeField becomeFirstResponder];
    NSLog(@"Active Field is %@",activeField);
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"Active Field is %@",activeField);
    [activeField resignFirstResponder];
    activeField = nil;
}


- (IBAction)tappedFacebook:(id)sender {
    
}

- (void) setupView
{
    self.txtFullName.layer.borderWidth = 1;
    self.txtContactNumber.layer.borderWidth = 1;
    self.txtEmail.layer.borderWidth = 1;
    self.txtPassword.layer.borderWidth = 1;
    self.txtConfirmPass.layer.borderWidth = 1;

    self.txtFullName.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtContactNumber.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtEmail.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtPassword.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtConfirmPass.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    
    self.txtFullName.layer.cornerRadius = 5;
    self.txtContactNumber.layer.cornerRadius = 5;
    self.txtEmail.layer.cornerRadius = 5;
    self.txtPassword.layer.cornerRadius = 5;
    self.txtConfirmPass.layer.cornerRadius = 5;
    
    self.txtFullName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Full Name" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtContactNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contact No" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"yourname@email.com" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtConfirmPass.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    self.btnSubmit.layer.cornerRadius = 5;

}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFullName) {
        [self.txtFullName resignFirstResponder];
        [self.txtContactNumber becomeFirstResponder];
    } else if (textField == self.txtContactNumber) {
        [self.txtContactNumber resignFirstResponder];
        [self.txtEmail becomeFirstResponder];
    } else if (textField == self.txtEmail) {
        [self.txtEmail resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
    } else if (textField == self.txtPassword) {
        [self.txtPassword resignFirstResponder];
        [self.txtConfirmPass becomeFirstResponder];
    } else if (textField == self.txtConfirmPass) {
        [self.txtConfirmPass resignFirstResponder];
    }
    return YES;
}


- (IBAction)tappedSubmit:(id)sender {
    NSString * strFullName = self.txtFullName.text;
    NSString * strContactNo = self.txtContactNumber.text;
    NSString * strEmail = self.txtEmail.text;
    NSString * strPassword = self.txtPassword.text;
    NSString * strConfirmPass = self.txtConfirmPass.text;
    
    NSString * errorMessage;
    
    if([strFullName isEqualToString:@""]||[strContactNo isEqualToString:@""]||[strEmail isEqualToString:@""]||[strPassword isEqualToString:@""]){
        errorMessage = @"Please fill all the above fields";
    }
    
    if (errorMessage){
        UIAlertView *alertDiag = [[UIAlertView alloc]initWithTitle:@"Fields Required" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertDiag show];
    }
    else{
    
        if(strPassword.length<8)
        {
            UIAlertView *alertDiag = [[UIAlertView alloc]initWithTitle:@"Password Too Short" message:@"Please enter password which has minimum of 8 characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertDiag show];
        }
        
        else{
            
            if(![strConfirmPass isEqualToString:strPassword])
            {
                UIAlertView *alertDiag = [[UIAlertView alloc]initWithTitle:@"Passwords mismatch" message:@"Please make sure that you have entered same passwords" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertDiag show];
            }
            
            else{
                [SVProgressHUD showWithStatus:@"Signing Up"];
                NSDictionary *params = @{@"name": strFullName,
                                         @"contact_no": strContactNo,
                                         @"email": strEmail,
                                         @"password": strPassword};
           
                [[APIClient sharedClient] POST:@"users" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    NSInteger statusCode = operation.response.statusCode;
                    NSString * strMessage = [responseObject objectForKey:@"message"];
                    if(statusCode == 201) {
                        UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Success" message:@"You have successfully signed up. Please check for confirmation email and Sign In after you have confirmed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alrtSuccess show];
                    }
                    else if(statusCode == 400) {
                        UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Failed" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alrtSuccess show];
                    }
                    [SVProgressHUD dismiss];
                    
          
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"The ERROR is %@",error);
                    NSInteger statusCode = operation.response.statusCode;
                    NSLog(@"STATUS CODE IS %li",(long)statusCode);
                    if(statusCode == 201) {
                        UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Success" message:@"You have successfully signed up. Please check for confirmation email and Sign In after you have confirmed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alrtSuccess show];
                    }
                    [SVProgressHUD dismiss];
                    }];
                }
            }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
