//
//  NotificationTableViewCell.m
//  BoxBoss
//
//  Created by ark on 24/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NotificationTableViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation NotificationTableViewCell
{
    UILabel * lblTitle;
    UILabel * lblsubTitle;
    UIImageView * imgViewNotif;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void) setupView{
    
    if (!imgViewNotif)
    {
        imgViewNotif = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10, 45, 45)];
        imgViewNotif.backgroundColor = [UIColor whiteColor];
        imgViewNotif.contentMode = UIViewContentModeScaleAspectFit;
        imgViewNotif.layer.cornerRadius = 5;
        imgViewNotif.clipsToBounds = YES;
        [self addSubview:imgViewNotif];
    }
    
    if (!lblTitle){
        lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(90, 10, 200, 20)];
        lblTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        lblTitle.textColor = [UIColor whiteColor];
        [self addSubview:lblTitle];
    }
    if (!lblsubTitle) {
        lblsubTitle = [[UILabel alloc]initWithFrame:CGRectMake(90, 40, 200, 20)];
        lblsubTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        lblsubTitle.textColor = [UIColor whiteColor];
        [self addSubview:lblsubTitle];
    }
    if (!imgViewNotif){
        
    }
}

- (void)loadViewWithNotif:(Notification *)notif{
    lblTitle.text = notif.name;
    lblsubTitle.text = notif.descNotif;
    [imgViewNotif setImageWithURL:[NSURL URLWithString:notif.image]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
