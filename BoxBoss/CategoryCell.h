//
//  CategoryCell.h
//  BoxBoss
//
//  Created by ark on 24/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoriesByLocation.h"

@interface CategoryCell : UICollectionViewCell

- (void) setupView;
- (void) loadViewWithCategoryObject:(CategoriesByLocation *)objCat;

@end
