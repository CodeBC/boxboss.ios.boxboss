//
//  NotificationTableViewCell.h
//  BoxBoss
//
//  Created by ark on 24/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notification.h"

@interface NotificationTableViewCell : UITableViewCell

- (void) setupView;
- (void) loadViewWithNotif:(Notification *)notif;

@end
