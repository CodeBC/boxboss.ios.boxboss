//
//  SignInSignUpViewController.m
//  BoxBoss
//
//  Created by ark on 26/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SignInSignUpViewController.h"
#import "SWRevealViewController.h"
#import "APIClient.h"
#import "UIColor+expanded.h"
#import "SVProgressHUD.h"
#import "SWRevealViewController.h"
#import "User.h"

@interface SignInSignUpViewController ()
{
    UITextField *activeField;
    NSString * userID;
}
@property (weak, nonatomic) IBOutlet UIButton *btnSignOut;
@property (weak, nonatomic) IBOutlet UIButton *btnForget;
@property (weak, nonatomic) IBOutlet UIButton *btnNewUser;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation SignInSignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog( @"### running FB sdk version: %@", [FBSettings sdkVersion] );
    [self setupView];
//    self.btnSignOut.enabled = YES;
    self.btnSignOut.hidden = YES;
    self.loginView.delegate = self;
    self.loginView.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    self.navigationItem.title = @"Sign In";
    

    _btnMenu.target = self.revealViewController;
    _btnMenu.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.txtUsername.delegate = self;
    self.txtPassword.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void) viewWillAppear:(BOOL)animated{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"authToken"] == nil) {
        [self showUIElements];
    }
    else {
        NSString * isFacebook = [defaults objectForKey:@"isFacebook"];
        [self hideUIElementsForFacebook:isFacebook];
    }
    [self registerForKeyboardNotifications];
}

- (void) viewWillDisappear:(BOOL)animated{
    [self deregisterFromKeyboardNotifications];
    [self dismissKeyboard];
}

- (void) hideUIElementsForFacebook:(NSString *)isFb{
    if ([isFb isEqualToString:@"YES"]){
        self.txtUsername.hidden = YES;
        self.txtPassword.hidden = YES;
        self.btnSubmit.hidden = YES;
        self.btnNewUser.hidden = YES;
        self.btnForget.hidden = YES;
    }
    else if([isFb isEqualToString:@"NO"]){
        self.txtUsername.hidden = YES;
        self.txtPassword.hidden = YES;
        self.btnSubmit.hidden = YES;
//        self.btnSubmit.enabled = NO;
        self.btnNewUser.hidden = YES;
        self.btnForget.hidden = YES;
        self.loginView.hidden = YES;
        self.statusLabel.hidden = YES;
//        self.btnSignOut.enabled = YES;
        self.btnSignOut.hidden = NO;
    }
    
}

- (void) showUIElements{
    self.txtUsername.hidden = NO;
    self.txtPassword.hidden = NO;
    self.btnSubmit.hidden = NO;
//    self.btnSubmit.enabled = YES;
    self.btnNewUser.hidden = NO;
    self.btnForget.hidden = NO;
//    self.btnSignOut.enabled = NO;
    self.btnSignOut.hidden = YES;
    self.loginView.hidden = NO;
    self.statusLabel.hidden = NO;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"Keyboard shown");
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    [activeField.superview setFrame:bkgndRect];

    if ((self.view.frame.size.height-(activeField.frame.origin.y+activeField.frame.size.height)) < kbSize.height){
        [self.scrollView setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
    }
}
    

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (activeField)
    {
     [activeField resignFirstResponder];
    }
    activeField = textField;
    [activeField becomeFirstResponder];
    NSLog(@"Active Field is %@",activeField);
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"Active Field is %@",activeField);
    [activeField resignFirstResponder];
    activeField = nil;
    
    
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    
    self.statusLabel.text = [NSString stringWithFormat:@"Logged in as: %@",user.name];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"authToken"] == nil) {
        [self sendInfoWithUser: user];
    }
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    self.statusLabel.text= @"Not logged in with Facebook";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"isFacebook %@",[defaults objectForKey:@"isFacebook"]);
    if ([[defaults objectForKey:@"isFacebook"]isEqualToString:@"YES"]) {
        [self showUIElements];
        [self clearAuthTokenFromFacebook:YES];
    }
}

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    self.statusLabel.text = @"Logged in as:";
    NSLog(@"LOGGED IN");
   // [self hideUIElementsForFacebook:@"YES"];
}

- (void) sendInfoWithUser:(id<FBGraphUser>)user{
    
    NSString * name = [user objectForKey:@"name"];
    NSString * FBid = [user objectForKey:@"id"];
    NSString * email = [user objectForKey:@"email"];
    
    [SVProgressHUD showWithStatus:@"Signing In"];
    
    NSDictionary *params = @{@"email": email,
                             @"name": name,
                             @"fb_id": FBid};
    
    [[APIClient sharedClient] POST:@"users/fb_login" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"The response is %@",responseObject);
       
        userID = [responseObject objectForKey:@"id"];
        NSString *authToken = [responseObject objectForKey:@"auth_token"];
        [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:@"authToken"];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"isFacebook"];
        [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"name"] forKey:@"fullName"];
        [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"contact_no"] forKey:@"contactNo"];
        [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"email"] forKey:@"email"];
        [[NSUserDefaults standardUserDefaults] synchronize];
       
        NSString * deviceToken = [[NSUserDefaults standardUserDefaults]
                                  stringForKey:@"deviceToken"];
        if ([deviceToken isEqual:NULL])
        {
         deviceToken = @"23429834jf39g83g7938";   
        }
        NSDictionary * paraToken = @{@"user_id": userID,
                                     @"type": @"I",
                                     @"token": deviceToken};
        [[APIClient sharedClient]POST:@"devices" parameters:paraToken success:^(AFHTTPRequestOperation *op, id response) {
            
        } failure:^(AFHTTPRequestOperation *op, NSError *err) {
            NSInteger status = op.response.statusCode;
            if (status == 201) {
                UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Success" message:@"You have successfully signed in" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alrtSuccess show];
                [SVProgressHUD dismiss];
            }
        }];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"The ERROR is %@",error);
        
    }];
    
    
}

- (void) clearAuthTokenFromFacebook:(BOOL)facebook{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"authToken"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isFacebook"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"fullName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"contactNo"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
   
    [[NSUserDefaults standardUserDefaults]synchronize ];
    if (!facebook) {
        UIAlertView *alrtLoggedOut = [[UIAlertView alloc]initWithTitle:@"Logged Out" message:@"You have successfully logged out" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alrtLoggedOut show];
    }
}

- (void) setupView{
    self.txtUsername.layer.borderWidth = 1;
    self.txtPassword.layer.borderWidth = 1;
    self.txtPassword.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtUsername.layer.borderColor = [[UIColor colorWithHexString:@"b75814"]CGColor];
    self.txtUsername.layer.cornerRadius = 5;
    self.txtPassword.layer.cornerRadius = 5;
    self.txtUsername.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"yourname@email.com" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.btnSubmit.layer.cornerRadius = 5;
    self.btnSignOut.layer.cornerRadius = 5;
}

- (IBAction)tappedSignOut:(id)sender {
    [self clearAuthTokenFromFacebook:NO];
    [self showUIElements];
}

- (IBAction)tappedFacebook:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/BoxBoss.sg"]];
}

- (IBAction)tappedSubmit:(id)sender {
    NSString * strUsername = self.txtUsername.text;
    NSString * strPassword = self.txtPassword.text;
    NSString * errorMessage;
    
    if ([strUsername isEqualToString:@""]){
        errorMessage = @"Please fill in registerd Email";
    }else if ([strPassword isEqualToString:@""]){
        errorMessage = @"Please fill in password";
    }
    else if([strUsername isEqualToString:@""]&&[strPassword isEqualToString:@""]){
        errorMessage = @"Please fill in Email and Password";
    }

    if (errorMessage){
    UIAlertView *alertDiag = [[UIAlertView alloc]initWithTitle:@"Fields Required" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertDiag show];
    }
    else{
        [SVProgressHUD showWithStatus:@"Signing In"];
        NSDictionary *params = @{@"email": strUsername,
                                 @"password": strPassword};
        
        [[APIClient sharedClient] POST:@"users/sign_in" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSInteger statusCode = operation.response.statusCode;

            /*
            NSString *authToken = [responseObject objectForKey:@"auth_token"];
            [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:@"authToken"];
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"isFacebook"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [SVProgressHUD dismiss];
            
            UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Success" message:@"You have successfully signed in" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alrtSuccess show];
             */
            if(statusCode == 200){
                NSLog(@"RESPONSE IS %@",responseObject);
                userID = [responseObject objectForKey:@"id"];
                NSLog(@"USER ID IS %@",userID);
                NSString *authToken = [responseObject objectForKey:@"auth_token"];
                [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:@"authToken"];
                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"isFacebook"];
                [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"name"] forKey:@"fullName"];
                [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"contact_no"] forKey:@"contactNo"];
                [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"email"] forKey:@"email"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSString * deviceToken = [[NSUserDefaults standardUserDefaults]
                                          stringForKey:@"deviceToken"];
                if ([deviceToken isEqual:NULL])
                {
                    deviceToken = @"23429834jf39g83g7938";
                }
                NSLog(@"Device token is %@ and user ID is %@",deviceToken, userID);
                
                NSDictionary * paraToken = @{@"user_id": userID,
                                             @"token": deviceToken};
                [[APIClient sharedClient]POST:@"devices" parameters:paraToken success:^(AFHTTPRequestOperation *op, id response) {

                    [SVProgressHUD dismiss];
                } failure:^(AFHTTPRequestOperation *op, NSError *err) {
                    NSLog(@"The error for device %@",err);
                       NSInteger status = op.response.statusCode;
                       if (status == 201) {
                           UIAlertView *alrtSuccess = [[UIAlertView alloc]initWithTitle:@"Success" message:@"You have successfully signed in" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                           [alrtSuccess show];

                       }
                    [SVProgressHUD dismiss];
                }];
               
            }
            else if(statusCode == 400) {
                UIAlertView *alrtWrong = [[UIAlertView alloc]initWithTitle:@"Invalid" message:@"Wrong username or password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alrtWrong show];
            }
            else if (statusCode == 401){
                UIAlertView *alrtConfirm = [[UIAlertView alloc]initWithTitle:@"Invalid" message:@"Please check your email and confirm your account first" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alrtConfirm show];
            }
            [SVProgressHUD dismiss];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"The ERROR is %@",error);
            [SVProgressHUD dismiss];
//            NSInteger statusCode = operation.response.statusCode;
//            if(statusCode == 400) {
//                UIAlertView *alrtWrong = [[UIAlertView alloc]initWithTitle:@"Invalid" message:@"Wrong username or password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alrtWrong show];
//            }
//            else if (statusCode == 401){
//                UIAlertView *alrtConfirm = [[UIAlertView alloc]initWithTitle:@"Invalid" message:@"Please check your email and confirm your account first" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alrtConfirm show];
//            }
            
        }];
       
    }
}


-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtUsername) {
        [self.txtUsername resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
    } else if (textField == self.txtPassword) {
        [self.txtPassword resignFirstResponder];
    }
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        SWRevealViewController *revealController = self.revealViewController;
        UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loyalty"];
        [revealController pushFrontViewController:navController animated:YES];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
