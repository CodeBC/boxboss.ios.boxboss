//
//  ProductFeedbackViewController.h
//  Box Boss
//
//  Created by ark on 6/5/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"

@interface ProductFeedbackViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) Products * objProd;
@property (weak, nonatomic) IBOutlet UITextField *txtFullName;
@property (weak, nonatomic) IBOutlet UITextField *txtContactNo;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextView *txtMessage;
@property (weak, nonatomic) IBOutlet UILabel *txtHeaderMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnPickOne;
@property (strong, nonatomic) UIActionSheet *menu;

@end
