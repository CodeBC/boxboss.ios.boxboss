//
//  NotificationViewController.m
//  BoxBoss
//
//  Created by ark on 24/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NotificationViewController.h"
#import "SWRevealViewController.h"
#import "APIClient.h"
#import "SVProgressHUD.h"
#import "NotificationTableViewCell.h"
#import "Notification.h"
#import "NotificationDetailViewController.h"

@interface NotificationViewController ()
{
    NSMutableArray * arrNotifications;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnMenu;
@property (weak, nonatomic) IBOutlet UITableView *tblNotifications;

@end

@implementation NotificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrNotifications = [[NSMutableArray alloc]init];

    [self loadData];
    self.navigationItem.title = @"Notifications";
    
    _btnMenu.target = self.revealViewController;
    _btnMenu.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self setupTable];
}

- (void) loadData{
    [SVProgressHUD showWithStatus:@"Loading"];
    [[APIClient sharedClient] GET:@"notifications" parameters:nil success: ^(AFHTTPRequestOperation *operation, id json){
        for (NSDictionary *dict in json){
            Notification * obj = [[Notification alloc]init];
            obj.notifID = [dict objectForKey:@"id"];
            obj.name = [dict objectForKey:@"title"];
            obj.descNotif = [NSString stringWithFormat:@"%@\nDate posted : %@\n\n%@", [dict objectForKey:@"message"], [dict objectForKey:@"date"], [dict objectForKey:@"content"]];
            obj.date = [dict objectForKey:@"date"];
            obj.image = [dict objectForKey:@"image"];
            [arrNotifications addObject:obj];
        }
        [self.tblNotifications reloadData];
        [SVProgressHUD dismiss];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"THE ERROR is %@",error);
        [SVProgressHUD dismiss];
    }];

}
   
- (void) setupTable
{
    self.tblNotifications.delegate = self;
    self.tblNotifications.dataSource = self;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNotifications.count;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Tapped %i",indexPath.row);
    Notification * objNotif;
    objNotif = [arrNotifications objectAtIndex:indexPath.row];
    NotificationDetailViewController * notifDetailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"notifDetail"];
    notifDetailVC.notif = objNotif;
    [self.navigationController pushViewController:notifDetailVC animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notif"];
    if (cell == nil) {
        cell = [[NotificationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    cell.backgroundColor = [UIColor clearColor];
    [cell setupView];
    [cell loadViewWithNotif:[arrNotifications objectAtIndex:indexPath.row]];
    
    return cell;

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
