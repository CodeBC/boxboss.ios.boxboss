//
//  LocationCell.m
//  BoxBoss
//
//  Created by ark on 24/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LocationCell.h"
#import "Location.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation LocationCell
{
    UILabel *lblLocationName;
    UIView *overlayView;
    UIImageView *imgViewArrow;
    UIImageView *imgViewLocation;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setupView{
    if (!imgViewLocation){
        self.backgroundColor = [UIColor whiteColor];
        imgViewLocation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 250, 100)];
        imgViewLocation.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:imgViewLocation];
    }
    
    if (!overlayView){
        overlayView = [[UIView alloc]initWithFrame:CGRectMake(0, 76, 256, 30)];
        overlayView.backgroundColor = [UIColor blackColor];
        overlayView.alpha = 0.5;
        [self addSubview:overlayView];
    }
    
    if (!imgViewArrow){
        imgViewArrow = [[UIImageView alloc]initWithFrame:CGRectMake(225, 80, 15, 15)];
        imgViewArrow.contentMode = UIViewContentModeScaleAspectFit;
        imgViewArrow.image = [UIImage imageNamed:@"arrow"];
        [self addSubview:imgViewArrow];
    }
    
    if (!lblLocationName){
        lblLocationName = [[UILabel alloc] initWithFrame:CGRectMake(10, 77, 200, 20)];
        lblLocationName.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
        lblLocationName.textColor = [UIColor whiteColor];
        [self addSubview:lblLocationName];
    }
}

- (void) loadViewWithLocationObject:(Location *)objLoc{
    [imgViewLocation setImageWithURL:[NSURL URLWithString:objLoc.locImgURL]];
    lblLocationName.text = objLoc.locName;
}

@end
