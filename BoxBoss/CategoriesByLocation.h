//
//  CategoriesByLocation.h
//  BoxBoss
//
//  Created by ark on 26/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoriesByLocation : NSObject

@property (nonatomic,strong) NSNumber * catID;
@property (nonatomic,strong) NSString * catName;
@property (nonatomic,strong) NSString * catImgURL;

@end
