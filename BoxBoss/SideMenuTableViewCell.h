//
//  SideMenuTableViewCell.h
//  BoxBoss
//
//  Created by ark on 25/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewCell : UITableViewCell

- (void) setupView;
- (void) loadViewWithItemName:(NSString *)name iconIndex:(NSUInteger)index;

@end
